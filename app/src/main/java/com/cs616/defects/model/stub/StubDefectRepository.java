package com.cs616.defects.model.stub;

import com.cs616.defects.model.Defect;
import com.cs616.defects.model.CRUDRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ian on 15-10-24.
 */
public class StubDefectRepository implements CRUDRepository<Long, Defect> {

    private static long currentId;
    private static Map<Long, Defect> db;

    static {
        currentId = 0L;
        db = new HashMap<>();
    }

    @Override
    public boolean create(Defect element) {
        db.put(++currentId, element);
        element.setId(currentId);
        return true;
    }

    @Override
    public Defect read(Long id) {
        if(db.containsKey(id))
            return db.get(id);
        else
            return null;
    }

    @Override
    public List<Defect> readAll() {
        return new ArrayList<>(db.values());
    }

    @Override
    public boolean update(Defect element) {
        db.put(element.getId(), element);
        return true;
    }

    @Override
    public boolean delete(Defect element) {
        db.remove(element);
        return true;
    }
}
