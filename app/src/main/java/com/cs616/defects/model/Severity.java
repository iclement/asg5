package com.cs616.defects.model;

/**
 * Created by ian on 15-10-08.
 */
public enum Severity {
    TRIVIAL, MINOR, MAJOR, SHOWSTOPPER
}
