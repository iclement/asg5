package com.cs616.defects.model;

/**
 * Created by ian on 15-10-08.
 */
public enum UserType {
    CUSTOMER, DEVELOPER, MANAGER, TESTER
}
