package com.cs616.defects.model;

/**
 * Created by ian on 15-10-03.
 */
public class User {

    /* Fields */

    private long id;
    private String name;
    private String imageUrl;
    private UserType userType;

    public User(String name, UserType userType, String imageUrl) {
        this.name = name;
        this.userType = userType;
        this.imageUrl = imageUrl;
    }

    /* Getters and Setters */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public User setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public UserType getUserType() {
        return userType;
    }

    public User setUserType(UserType userType) {
        this.userType = userType;
        return this;
    }

    /* helper for .equals() */
    private static boolean nullOrEqual(Object o1, Object o2) {
        try {
            // returns true if both o1 and o2 are null, or else they are equal
            // note: if o1 == o2 then o1.equals(o2) by properties of equality.
            return (o1 == o2) || (o1.equals(o2));
        }
        catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public boolean equals(Object rhsObj) {
        User rhs = (User) rhsObj;
        return nullOrEqual(this.id,  rhs.id)
                && nullOrEqual(this.name, rhs.name)
                && nullOrEqual(this.imageUrl, rhs.imageUrl);
    }
}
