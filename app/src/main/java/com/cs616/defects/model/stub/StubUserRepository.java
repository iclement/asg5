package com.cs616.defects.model.stub;

import com.cs616.defects.model.CRUDRepository;
import com.cs616.defects.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ian on 15-10-25.
 */
public class StubUserRepository implements CRUDRepository<Long, User> {

    private static long currentId;
    private static Map<Long, User> db;

    static {
        currentId = 0L;
        db = new HashMap<>();
    }

    @Override
    public boolean create(User element) {
        db.put(++currentId, element);
        element.setId(currentId);
        return true;
    }

    @Override
    public User read(Long id) {
        if(db.containsKey(id))
            return db.get(id);
        else
            return null;
    }

    @Override
    public List<User> readAll() {
        return new ArrayList<>(db.values());
    }

    @Override
    public boolean update(User element) {
        db.put(element.getId(), element);
        return true;
    }

    @Override
    public boolean delete(User element) {
        db.remove(element.getId());
        return true;
    }
}
