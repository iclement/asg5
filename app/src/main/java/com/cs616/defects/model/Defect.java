package com.cs616.defects.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by ian on 15-10-03.
 */
public class Defect {

    /* Fields */

    private long id;
    private Date created;
    private Date modified;
    private String summary;
    private Status status;
    private User assignedTo;
    private User createdBy;
    private Severity severity;

    /* Getters and Setters */

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public Defect setCreated(Date created) {
        this.created = created;
        return this;
    }

    public Date getModified() {
        return modified;
    }

    public Defect setModified(Date modified) {
        this.modified = modified;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public Defect setSummary(String summary) {
        this.summary = summary;
        return this;

    }

    public Status getStatus() {
        return status;
    }

    public Defect setStatus(Status status) {
        this.status = status;
        return this;
    }


    public Severity getSeverity() {
        return severity;
    }

    public Defect setSeverity(Severity severity) {
        this.severity = severity;
        return this;
    }

    public User getAssignedTo() {
        return assignedTo;
    }

    public Defect setAssignedTo(User assignedTo) {
        this.assignedTo = assignedTo;
        return this;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public Defect setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
        return this;
    }


    /* helper for .equals() */
    private static boolean nullOrEqual(Object o1, Object o2) {
        try {
            // was (o1 == null && o2 == null) || (o1.equals(o2));
            // returns true if: o1 == null and o2 == null,
            // shortcut: if o1 == o2 then o1.equals(o2) by property of equality.
            return (o1 == o2) || (o1.equals(o2));
        }
        catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public boolean equals(Object rhsObj) {
        Defect rhs = (Defect) rhsObj;

        List<Boolean> tests = new LinkedList<>();
        tests.add( nullOrEqual(this.created,    rhs.created) );
        tests.add( nullOrEqual(this.status,     rhs.status) );
        tests.add( nullOrEqual(this.summary,    rhs.summary) );
        tests.add( nullOrEqual(this.modified,   rhs.modified) );
        tests.add( nullOrEqual(this.id,         rhs.id));
        tests.add( nullOrEqual(this.createdBy,  rhs.createdBy) );
        tests.add( nullOrEqual(this.assignedTo, rhs.assignedTo));

        return !tests.contains(false);
    }

}
